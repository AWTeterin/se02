Software
---
    -JRE
    -Java 1.8
    -Maven 4.0.0

Technology Stack
---
    -Maven
    -Git

Developer
---
    Teterin Alexei
    email: <teterin2012@rambler.ru>

Commands for building the app
---
    mvc clean
    mvc install
    mvc clean install

Commands for run the app
---
    java -jar target/SE02-1.0-SNAPSHOT.jar